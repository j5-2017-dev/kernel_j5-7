1. How to Build
	- get Toolchain
		From android git server, codesourcery and etc ..
			- aarch64-linux-android-4.9
	
	- edit Makefile
		edit "CROSS_COMPILE" to right toolchain path(You downloaded).
			- CROSS_COMPILE = android/prebuilts/gcc/linux-x86/aarch64/aarch64-linux-android-4.9/bin/aarch64-linux-android-
	- to Build
		$ export ANDROID_MAJOR_VERSION=o
		$ make j5y17lte_00_defconfig
		$ make -j64
		
2. Output files
	- Kernel : arch/arm/boot/zImage
	- module : drivers/*/*.ko
	
3. How to Clean
	$ make clean

$ make distclean